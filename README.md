#  Adapter Pattern  #

##  Description

It is one of the structural design pattern.
Helping to class that be unable to work together, to be able to work together.

### Structure of Adapter Pattern 

* Class Target - is Class or Interface that have method that client can use to do works.

* Class Client -is Class that use methods in Target class

* Class Adaptee - is Class  that needs adapting.

* Class Adapter - is Class that call methods in Adaptee by they call in their own method,method in Adapter will called by Client.

### There are two types of adapter patterns 

 1. Class Adapter – use properties of OOP by inheritance  and extends the source interface.

UML-Class Adapter


![class_adapter](http://fuzzduck.com/images/ClassAdapterUML.gif)

 2. Object Adapter – Composition , the adapter contains the source object.

UML- Object Adapter

![object_adapter](http://fuzzduck.com/images/ObjectAdapterUML.gif)

##  Example Code

### Adaptee 

```
#!Java
//Class Adaptee
public class Meter {
	double lengthMeter;
	public double getLenght(){
		return this.lengthMeter;
	}
	
	public void setLenght(double lengthMeter){
		this.lengthMeter = lengthMeter;
	}

}
```

### Target
```
#!Java
// Class Target
public interface LengthInfo {
	public double getLengthInCentimeter();
	public void setLengthInCentimeter(double lengthCentimeter);
	public double getLengthInMeter();
	public void setLengthInMeter(double lengthMeter);
}
```

### Class Adapter
```
#!Java
//Class adapter
public class LengthClassAdapter extends Meter implements LengthInfo{

	@Override
	public double getLengthInCentimeter() {
	return this.meterToCentimeter(lengthMeter);
	}

	@Override
	public void setLengthInCentimeter(double lengthCentimeter) {
			this.lengthMeter = this.centimeterToMeter(lengthCentimeter);
		
	}

	@Override
	public double getLengthInMeter() {
		return this.lengthMeter;
	}

	@Override
	public void setLengthInMeter(double lengthMeter) {
		this.lengthMeter = lengthMeter;
		
	}
	private double meterToCentimeter(double lengthMeter){
		return lengthMeter/(1e-2);
	}
	
	private double centimeterToMeter(double lengthCentimeter){
		return lengthCentimeter*(1e-2);
	}
}

```
### Object Adapter

```
#!Java

// Class Object adapter
public class LengthObjectAdapter implements LengthInfo{
	private Meter meter; 
	public LengthObjectAdapter(){
		meter  = new Meter();
	}
	@Override
	public double getLengthInCentimeter() {
		return meterToCentimeter(meter.getLenght());
	}

	@Override
	public void setLengthInCentimeter(double lengthCentimeter) {
			meter.setLenght(centimeterToMeter(lengthCentimeter));
		
	}

	@Override
	public double getLengthInMeter() {
		return meter.lengthMeter;
	}

	@Override
	public void setLengthInMeter(double lengthMeter) {
		meter.lengthMeter = lengthMeter;
		
	}
	private double meterToCentimeter(double lengthMeter){
		return lengthMeter/(1e-2);
	}
	
	private double centimeterToMeter(double lengthCentimeter){
		return lengthCentimeter*(1e-2);
	}
}

```
### Client

```
#!Java
//this is Client Class
public class App {
		public static void main(String[] args) {

			System.out.println("class adapter test");
			LengthInfo ClenghtInfo = new LengthClassAdapter();
			ClenghtInfo.setLengthInMeter(100);
			System.out.println("Lenght in meter:" + ClenghtInfo.getLengthInMeter());
			System.out.println("Lenght in Centimeter:" + ClenghtInfo.getLengthInCentimeter());
			ClenghtInfo.setLengthInCentimeter(100);
			System.out.println("Lenght in meter:" + ClenghtInfo.getLengthInMeter());
			System.out.println("Lenght in Centimeter:" + ClenghtInfo.getLengthInCentimeter());

			System.out.println("\nobject adapter test");
			LengthInfo OlenghtInfo = new LengthClassAdapter();
			OlenghtInfo.setLengthInMeter(100);
			System.out.println("Lenght in meter:" + OlenghtInfo.getLengthInMeter());
			System.out.println("Lenght in Centimeter:" + OlenghtInfo.getLengthInCentimeter());
			OlenghtInfo.setLengthInCentimeter(100);
			System.out.println("Lenght in meter:" + OlenghtInfo.getLengthInMeter());
			System.out.println("Lenght in Centimeter:" + OlenghtInfo.getLengthInCentimeter());
			
		}

}
```


##  Exercise

use class below to turn on computer class by using adapter pattern.
computer  use 110V.but we give voltage = 220 v.

```
#!java
class MainApp {
	  final static Volt volt = new Volt(110);
      public static void main(String[] args) {
    	  Adapter adapter = new Adapter(volt);
    	  Computer comp = new Computer(adapter);
    	  comp.powerOn();
 
      }
}
```



```
#!java

class Volt {
 
	private int voltage;
 
	public Volt(int voltage) {
		this.voltage = voltage;
	}
	public int getVoltage() {
		return voltage;
	}
  }
```

```
#!java

class Adapter {
 
	public Adapter(Volt volt ) {
		
	}
	public int getVoltage() {
		//TODO do work
		return 0;
	}
}

interface Target {
	 public int getVoltage();
}
 
```

```
#!java

class Computer {
 
	private int ComVoltages;
	private Target target;
 
	public Computer(Target target) {
		target = target;
		ComVoltages = target.getVoltage();
	}
	public void powerOn() { 
		if(ComVoltages == 110)
			System.out.println("Power On"); 
	}
}
```